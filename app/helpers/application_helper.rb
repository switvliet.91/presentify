module ApplicationHelper
  def nav_link(link_text, link_path)
    class_name = current_page?(link_path) ? 'active' : ''
    if request.path.include?('/cv') && link_path == '/cv'
      class_name = 'active success-color'
    end

    content_tag(:li, class: 'main nav-item ' + class_name) do
      link_to link_path, class: 'main nav-link ' + class_name do
        icon = self.nav_icon(link_text)
        icon + link_text
      end
    end
  end

  def nav_icon(link_text)
    if link_text == 'Dashboard'
      fa_icon = 'nav-icon fa fa-home'
    elsif link_text == 'Blog'
      fa_icon = 'nav-icon fa fa-book'
    elsif link_text == 'CV'
      fa_icon = 'nav-icon fa fa-briefcase'
    elsif link_text == 'Inspiraties'
      fa_icon = 'nav-icon fa fa-eye'
    end
    content_tag(:i, "", class: fa_icon)
  end

  def tab_link(link_text, link_path)
    if request.path.include?('/cv')
      class_name = current_page?(link_path) ? 'active' : ''
    else
      class_name = ''
    end

    content_tag(:li, class: 'nav-item ' + class_name) do
      link_to link_path, class: 'nav-link ' + class_name do
        icon = self.tabnav_icon(link_text, link_path)
        icon + link_text
      end
    end
  end

  def tabnav_icon(link_text, link_path)
    if link_text == 'Profiel'
      fa_icon = 'subnav-icon fa fa-user'
    elsif link_text == 'Werkervaring'
      fa_icon = 'subnav-icon fa fa-briefcase'
    elsif link_text == 'Opleidingen'
      fa_icon = 'subnav-icon fa fa-leanpub'
    elsif link_text == 'Certificaten'
      fa_icon = 'subnav-icon fa fa-graduation-cap'
    elsif link_text == 'Vaardigheden'
      fa_icon = 'subnav-icon fa fa-list-ol'
    end
    content_tag(:i, "", class: fa_icon)
  end
end
