class Blog::PagesController < ApplicationController
  before_action :set_title

  def dashboard
    @articles = current_user.articles
  end

  private
  def set_title
    @title = 'Blog'
  end
end
