class Blog::ArticlesController < ApplicationController
  before_action :set_title

  def index
    @articles = current_user.articles
  end

  def show
    @article = Article.find(params[:id])
  end

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(article_params)
    @article.user_id = current_user.id
    if @article.save
      redirect_to blog_root_path
    else
      render :new
    end
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
  end

  def destroy
  end

  private

  def set_title
    @title = 'Artikelen'
  end

  def article_params
    params.require(:article).permit(:title, :summary, :content, :image)
  end
end
