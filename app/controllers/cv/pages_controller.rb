class Cv::PagesController < ApplicationController
  before_action :set_title

  def dashboard
  end

  private
  def set_title
    @title = 'Curriculum Vitae'
  end
end
