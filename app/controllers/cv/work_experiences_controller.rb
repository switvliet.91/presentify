class Cv::WorkExperiencesController < ApplicationController
  before_action :set_title
  helper_method :sort_column, :sort_direction

  def index
    @work_experiences = WorkExperience.all
  end

  def show
    @work_experience = WorkExperience.find(params[:id])
  end

  def new
    @work_experience = WorkExperience.new
  end

  def create
    @work_experience = WorkExperience.new(work_experience_params)
    @work_experience.user_id = current_user.id
    if @work_experience.save
      redirect_to cv_work_experiences_path
    else
      render :new
    end
  end

  def edit
  end

  def update
  end

  def destroy
  end

  private

  def work_experience_params
    params.require(:work_experience).permit(:job_title, :start_date, :end_date, :description)
  end

  def set_title
    @title = 'Mijn Werkervaringen'
  end

  def sortable_columns
    ["name", "price"]
  end

  def sort_column
    sortable_columns.include?(params[:column]) ? params[:column] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

end
