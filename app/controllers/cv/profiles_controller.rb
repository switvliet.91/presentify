class Cv::ProfilesController < ApplicationController
  before_action :set_title

  def index
  end

  def show
    @profile = current_user.profile
  end

  def new
    @profile = Profile.new
  end

  def create
    @profile = Profile.new(profile_params)
    @profile.user_id = current_user.id
    if @profile.save
      redirect_to cv_profiles_path
    else
      render :new
    end
  end

  def edit
  end

  def update
  end

  def destroy
  end

  private

  def set_title
    @title = 'Mijn Profiel'
  end

  def profile_params
    params.require(:profile).permit(:firstname, :lastname, :email,  :date_of_birth, :mobile)
  end

end
