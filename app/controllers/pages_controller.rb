class PagesController < ApplicationController
  before_action :set_title
  before_action :authenticate_user!

  def dashboard

  end

  private
  def set_title
    @title = 'Dashboard'
  end
end
