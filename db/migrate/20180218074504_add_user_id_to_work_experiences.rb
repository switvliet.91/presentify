class AddUserIdToWorkExperiences < ActiveRecord::Migration[5.1]
  def change
    add_reference :work_experiences, :user, foreign_key: true
  end
end
