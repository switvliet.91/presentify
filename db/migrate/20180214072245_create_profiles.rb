class CreateProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :profiles do |t|
      t.string :firstname
      t.string :lastname
      t.string :email
      t.date :date_of_birth
      t.string :phone
      t.string :mobile

      t.timestamps
    end
  end
end
