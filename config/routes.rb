Rails.application.routes.draw do
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }
  root to: 'pages#dashboard'
  namespace :cv do
    root to: 'pages#dashboard'
    resources :profiles
    resources :work_experiences
    resources :educations
    resources :certificates
    resources :skills
  end
  namespace :inspiration do
    root to: 'pages#dashboard'
    resources :technologies
    resources :tools
  end

  namespace :blog do
    root to: 'pages#dashboard'
    resources :articles
  end

end
